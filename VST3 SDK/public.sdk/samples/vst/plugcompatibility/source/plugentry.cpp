//------------------------------------------------------------------------
// Project     : VST SDK
// Version     : 3.5.0
//
// Category    : Examples
// Filename    : public.sdk/samples/vst/multiplugsupport/source/plugentry.cpp
// Created by  : Steinberg, 02/2011
// Description : MultiPLugSupport Example for VST 3
//
//-----------------------------------------------------------------------------
// LICENSE
// (c) 2012, Steinberg Media Technologies GmbH, All Rights Reserved
//-----------------------------------------------------------------------------
// This Software Development Kit may not be distributed in parts or its entirety  
// without prior written agreement by Steinberg Media Technologies GmbH. 
// This SDK must not be used to re-engineer or manipulate any technology used  
// in any Steinberg or Third-party application or software module, 
// unless permitted by law.
// Neither the name of the Steinberg Media Technologies nor the names of its
// contributors may be used to endorse or promote products derived from this 
// software without specific prior written permission.
// 
// THIS SDK IS PROVIDED BY STEINBERG MEDIA TECHNOLOGIES GMBH "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL STEINBERG MEDIA TECHNOLOGIES GMBH BE LIABLE FOR ANY DIRECT, 
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#include "plug.h"
#include "plugcontroller.h"
#include "plugcids.h"	// for class ids
#include "version.h"	// for versioning

#include "public.sdk/source/main/pluginfactoryvst3.h"

#define stringPluginAName "Plug A VST3"
#define stringPluginBName "Plug B VST3"

#if PLUGIN_A
#define stringPluginName stringPluginAName
#else
#define stringPluginName stringPluginBName
#endif

//------------------------------------------------------------------------
//  Module init/exit
//------------------------------------------------------------------------

//------------------------------------------------------------------------
// called after library was loaded
bool InitModule ()   
{
	return true; 
}

//------------------------------------------------------------------------
// called after library is unloaded
bool DeinitModule ()
{
	return true; 
}

using namespace Steinberg;
using namespace Steinberg::Vst;

#if PLUGIN_B // Plug B can support Plug A

#include "base/source/fobject.h"
#include "pluginterfaces/base/ipluginbase.h"
#include "pluginterfaces/vst/vstfuture.h"

namespace Steinberg {
namespace Vst {

//----VST 3.x--------------------------------
DEF_CLASS_IID (IPluginCompatibility)
}
}

//------------------------------------------------------------------------
class PluginCompatibility: public FObject,
						 public IPluginBase,
						 public IPluginCompatibility
{
public:
	//---from IPluginBase
	tresult PLUGIN_API initialize (FUnknown* context) { return kResultOk; }
	tresult PLUGIN_API terminate () { return kResultOk; }

	//---from IPluginCompatibility
	int32 PLUGIN_API getPluginCompatibilityCount (TUID processorUID);
	tresult PLUGIN_API getPluginCompatibilityInfo (TUID processorUID, int32 index, SPluginCompatibilityInfo* info);

	//---Interface---------
	OBJ_METHODS (PluginCompatibility, FObject)
	DEFINE_INTERFACES
		DEF_INTERFACE (IPluginBase)
		DEF_INTERFACE (IPluginCompatibility)
	END_DEFINE_INTERFACES (FObject)
	REFCOUNT_METHODS(FObject)
};

//------------------------------------------------------------------------
int32 PLUGIN_API PluginCompatibility::getPluginCompatibilityCount (TUID cid)
{
	FUID uid (cid);
	if (uid == PlugProcessorUID)
		return 1;
	return 0;
}

//------------------------------------------------------------------------
tresult PLUGIN_API PluginCompatibility::getPluginCompatibilityInfo (TUID processorUID, int32 index, SPluginCompatibilityInfo* info)
{
	FUID uid (processorUID);
	if (uid == PlugProcessorUID && index == 0 && info)
	{
		TUID lcid = INLINE_UID_FROM_FUID(PlugAProcessorUID); 
		SPluginCompatibilityInfo plugAClass (lcid, STR (stringPluginBName));
		
		memcpy (info, &plugAClass, sizeof (SPluginCompatibilityInfo));
		
		return kResultOk;
	}
	return kResultFalse;
}

//------------------------------------------------------------------------
static FUnknown* createCompatibilityInstance (void* context)
{
	return (IPluginCompatibility*)new PluginCompatibility;
}
#endif

//------------------------------------------------------------------------
//  VST Plug-in Entry
//------------------------------------------------------------------------
// Windows: do not forget to include a .def file in your project to export
// GetPluginFactory function!
//------------------------------------------------------------------------

BEGIN_FACTORY_DEF ("Steinberg Media Technologies", 
			   "http://www.steinberg.net", 
			   "mailto:info@steinberg.de")

	//---First Plug-in included in this factory-------
	// its kVstAudioEffectClass component
	DEF_CLASS2 (INLINE_UID_FROM_FUID(PlugProcessorUID),
				PClassInfo::kManyInstances,	// cardinality  
				kVstAudioEffectClass,		// the component category (dont changed this)
				stringPluginName,			// here the Plug-in name (to be changed)
				Vst::kDistributable,	// means that component and controller could be distributed on different computers
				"Fx",					// Subcategory for this Plug-in (to be changed)
				FULL_VERSION_STR,		// Plug-in version (to be changed)
				kVstVersionString,		// the VST 3 SDK version (dont changed this, use always this define)
				Steinberg::Vst::Plug::createInstance)	// function pointer called when this component should be instanciated

	// its kVstComponentControllerClass component
	DEF_CLASS2 (INLINE_UID_FROM_FUID (PlugControllerUID),
				PClassInfo::kManyInstances,  // cardinality   
				kVstComponentControllerClass,// the Controller category (dont changed this)
				stringPluginName "Controller",	// controller name (could be the same than component name)
				0,						// not used here
				"",						// not used here
				FULL_VERSION_STR,		// Plug-in version (to be changed)
				kVstVersionString,		// the VST 3 SDK version (dont changed this, use always this define)
				Steinberg::Vst::PlugController::createInstance)// function pointer called when this component should be instanciated

#if PLUGIN_B
	DEF_CLASS2 (INLINE_UID_FROM_FUID(PluginCompatibilityUID),
				PClassInfo::kManyInstances,	// cardinality  
				kVstCompatibilityClass,		// the component category (dont changed this)
				stringPluginName,			// here the Plug-in name (to be changed)
				Vst::kDistributable,	// means that component and controller could be distributed on different computers
				"Fx",					// Subcategory for this Plug-in (to be changed)
				FULL_VERSION_STR,		// Plug-in version (to be changed)
				kVstVersionString,		// the VST 3 SDK version (dont changed this, use always this define)
				createCompatibilityInstance)	// function pointer called when this component should be instanciated
#endif
END_FACTORY
