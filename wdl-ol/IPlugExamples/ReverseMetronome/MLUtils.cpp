#include "MLUtils.h"
#include <opencv2/core/core.hpp> 
#include <opencv2/ml/ml.hpp>

MLUtils::MLUtils(void)
  : arraysSet(false), velocitiesIncluded(false), filename("C:\\output_from_tests.txt")
{
	for(int i = 0; i < 128; ++i) {
			noteTempos[i] = 0.0;
			noteVelocities[i] = 0.0;
	}
}


MLUtils::~MLUtils(void)
{
}


void MLUtils::setMatrixArrays(std::map<int, long double> *tempos) {
	std::map<int, long double>::iterator it = tempos->begin();	
	for(; it != tempos->end(); ++it)
		noteTempos[it->first] = it->second;
	arraysSet = true;
}

void MLUtils::setMatrixArrays(std::map<int, long double> *tempos, std::map<int, long double> *velocities) {
	std::map<int, long double>::iterator it = tempos->begin();	
	for(; it != tempos->end(); ++it) {
			noteTempos[it->first] = it->second;
			noteVelocities[it->first] = velocities->at(it->first);
	}
	arraysSet = true;
}

Mat MLUtils::getSongMatrix() {
	//assert(arraysSet);
	//Mat temp = Mat();
	/*
	if(velocitiesIncluded) {
		long double data[128][2];
		for(int i = 0; i < 128; ++i) {
			data[i][0] = noteTempos[i];
			data[i][1] = noteVelocities[i];
		}
		temp = Mat(128, 2, CV_64F, data).inv();
	}*/
	//return Mat(1, 128, CV_64FC1, noteTempos);
	return Mat();
	//return Mat(Size(128, 1), CV_64F);//.push_back(Mat(1, 128, CV_64F, noteTempos));
}

void MLUtils::writeMatrixToFile() {
	std::ofstream outputFile("C:\\testData.txt" , std::ios_base::app);
	//Mat M = getSongMatrix();
	outputFile.app;
	for(int i = 0; i < 128; ++i) {
		outputFile << (long double) noteTempos[i] << std::endl;
	}
	//for(int i = 0; i < M.rows; ++i) {
	//	for(int j = 0; i < M.cols; ++j) {
		//	outputFile << (long double) M.at<long double>(i,j) << std::endl;
	//	}
	//}
	outputFile.close();
}

float* MLUtils::getTrainingMatrix() {
	//Mat M = Mat();
	std::ifstream matrixFile(filename);
	int matrixRows = 0;
	float matrix[657][128];
	while(!matrixFile.eof()) {
		for(int i = 0; i < 128; ++i) {
			matrixFile >> matrix[matrixRows][i];
			if(matrixFile.eof())
			{
				break;
			}
		}
		if(matrixFile.eof())
		{
			break;
		}
		//M.push_back(Mat(128, 1, CV_64F, currentRow).inv());
		matrixRows = matrixRows + 1;
	}
	matrixFile.close();
	std::ifstream yFile("C:\\y_values.txt");
	float y_list[657][1];
	int yRows = 0;
	while(!yFile.eof()) {
	yFile >> y_list[yRows][1];
	if(yFile.eof())
	{
		break;
	}
	yRows = yRows + 1;
	}
	yFile.close();
	Mat trainingDataMat(matrixRows, 128, CV_32FC1, matrix);
	Mat answers(yRows, 1, CV_32FC1, y_list);
	Mat M = Mat();
	float testdata [1][128];
	for(int i = 0; i < 128; ++i) {
		testdata[1][i] = noteTempos[i];
	}
	Mat sampleMat(1, 128, CV_32FC1, testdata);

	CvKNearest KNearest;
	//exit(1);
	KNearest.train(trainingDataMat, answers, M, false, 3, false);
	float response = KNearest.find_nearest(sampleMat, 3, 0, 0, 0, 0);
	//exit(1);
	
	
	/*
	CvSVMParams params;
    params.svm_type    = CvSVM::C_SVC;
    params.kernel_type = CvSVM::LINEAR;
    params.term_crit   = cvTermCriteria(CV_TERMCRIT_ITER, 100, 1e-6);
	CvSVM SVM;
	//exit(1);
    SVM.train(trainingDataMat, answers, Mat(), Mat(), params);
	exit(1);
	double response = SVM.predict(sampleMat);
	//exit(1);
	*/

	/*std::ofstream outputFile("C:\\GiveMe120.txt");
	for (int i = 0; i < 5; ++i)
	{
		for (int j = 0; j < 128; ++j)
		{
			outputFile << trainingDataMat.at<long double>(i,j);
		}
		outputFile << std::endl;
	}
	outputFile << "plz write here";
	outputFile.close();*/
	
	std::ofstream outputfile("c:\\giveme120.txt");
	outputfile << response << std::endl;
	outputfile.close();
	return *matrix;
	//return M;
	//return Mat();
	//return Mat::zeros(512, 512, CV_8UC3);
}
