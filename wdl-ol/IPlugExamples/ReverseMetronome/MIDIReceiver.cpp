#include "MIDIReceiver.h"

void MIDIReceiver::onMessageReceived(IMidiMsg* midiMessage) {
    IMidiMsg::EStatusMsg status = midiMessage->StatusMsg();
    // We're only interested in Note On/Off messages (not CC, pitch, etc.)
    if(status == IMidiMsg::kNoteOn) {
		// if there is a note on or note off message then add to 
		// MIDI message queue
				
        mMidiQueue.Add(midiMessage);
    }
}

void MIDIReceiver::advance() {
    while (!mMidiQueue.Empty()) {
		// if we have a message (key on or off)

		// creating a pointer to midiMessage that was 
		// received/created on line 10 of this cpp file
		// and assigning to it last msg in the queue
        IMidiMsg* midiMessage = mMidiQueue.Peek();
		// if object of midiMessage called mOffset is greater then the 
		// current mOffset received, we don't want to receive it 
		// (old part that we already received).
		// TODO - what does the int mOffset stands for?
        if (midiMessage->mOffset > mOffset) break;

        IMidiMsg::EStatusMsg status = midiMessage->StatusMsg();
        int noteNumber = midiMessage->NoteNumber();
        int velocity = midiMessage->Velocity();
        // There is only note on/off messages in the queue, see ::OnMessageReceived
        if (status == IMidiMsg::kNoteOn && velocity) {
            if(mKeyStatus[noteNumber] == false) {
                mKeyStatus[noteNumber] = true;
                mNumKeys += 1;
            }
            // A key pressed later overrides any previously pressed key:
            //if (noteNumber != mLastNoteNumber) {

							// Uncomment for sound...
							//mLastNoteNumber = noteNumber;
							//mLastFrequency = noteNumberToFrequency(mLastNoteNumber);
							//mLastVelocity = velocity;

							if(status == IMidiMsg::kNoteOn && mapLock == false) {
								// Add note to mMap
								//clock_t t = clock();
								//t = (float) (((float)t)/CLOCKS_PER_SEC * 1000.0);
								struct timeb tmb;
								ftime(&tmb);
								//tmb.seconds;
								long double t = (long double) (tmb.millitm/1000.000f);// + (tmb.time));
								//t += (tmb.time);
								long double t2 = (long double)tmb.time;
								t = (long double) (t + t2);
								//t = (long double) tmb.time;
								//t += 0.500f;
								//t *= 1000; 
								// Check if note exists in our map, add first note if first
								if(mMap.empty() || mMap.find(noteNumber) == mMap.end()) {
										std::vector<Note> mVector;
										// assigning the noteNumber, velocity, floor time to the vector  
										mVector.push_back(Note(noteNumber, velocity, (long double)t));
										// assigning that vector to the map
										mMap[noteNumber] = mVector;
								// Note exists in the mMap
								} else if(mMap.find(noteNumber) != mMap.end()) {				
										// always substracting from the first note played
										int size = mMap[noteNumber].size();
										//t = (long double) t - mMap[noteNumber].at(0).mTime;
										for(int i = 0; i < size; ++i) {
											t = (long double) t - mMap[noteNumber].at(i).mTime;
										}
										//t = 1000;
										// assigning the noteNumber, velocity, floor time to the vector  
										mMap[noteNumber].push_back(Note(noteNumber, velocity, (long double)t)); 
								}
							}
        } else {
            if(mKeyStatus[noteNumber] == true) {
                mKeyStatus[noteNumber] = false;
                mNumKeys -= 1;
            }
            // If the last note was released, nothing should play:
            if (noteNumber == mLastNoteNumber) {
                mLastNoteNumber = -1;
                mLastFrequency = -1;
                mLastVelocity = 0;
            }
        }
        mMidiQueue.Remove();
    }
    mOffset++;
}

void MIDIReceiver::clearMap() {
	for(int i = 0; i < mMap.size(); ++i) {
		mMap[i].clear();
	}
	mMap.clear();
}