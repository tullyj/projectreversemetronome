#include "ReverseMetronome.h"
#include "IPlug_include_in_plug_src.h"
#include "IControl.h"
#include "resource.h"
using namespace std;

const int kNumPrograms = 1;

enum EParams
{
    kNumParams
};

enum ELayout
{
    kWidth = GUI_WIDTH,
    kHeight = GUI_HEIGHT
};

ReverseMetronome::ReverseMetronome(IPlugInstanceInfo instanceInfo)
    :   IPLUG_CTOR(kNumParams, kNumPrograms, instanceInfo), finishedSong(false) {
    TRACE;

    IGraphics* pGraphics = MakeGraphics(this, kWidth, kHeight);
		pGraphics->AttachPanelBackground(&COLOR_BLACK);
		// Default tempo message
		calcMessage = "[Calculating tempo...]";
		controlOneMessage = calcMessage;
		// Default display message
		controlTwoMessage = "Project ReverseMetronome";
		controlThreeMessage = "";
		// Position of first line of text
		IRECT tmpRect4(0, 60, 300, 120);
		// Position of second line of text
		IRECT tmpRect5(0, 84, 300, 120);
		// Position of second line of text
		IRECT tmpRect6(0, 108, 300, 120);
		// Printing first line of text
		IText textProps4(24, &COLOR_ORANGE, "Arial", IText::kStyleNormal, IText::kAlignCenter, 0, IText::kQualityDefault);
		controlOne = pGraphics->AttachControl(new ITextControl(this, tmpRect4, &textProps4, controlOneMessage));
		// Printing second line of text
		IText textProps5(16, &COLOR_GRAY, "Arial", IText::kStyleNormal, IText::kAlignCenter, 0, IText::kQualityDefault);
		controlTwo = pGraphics->AttachControl(new ITextControl(this, tmpRect5, &textProps5, controlTwoMessage));
		// Printing third line of text
		IText textProps6(16, &COLOR_GRAY, "Arial", IText::kStyleNormal, IText::kAlignCenter, 0, IText::kQualityDefault);
		controlThree = pGraphics->AttachControl(new ITextControl(this, tmpRect6, &textProps6, controlThreeMessage));
		AttachGraphics(pGraphics);
    CreatePresets();
		mMIDIReceiver.mapLock = false;
}

ReverseMetronome::~ReverseMetronome() {}

void ReverseMetronome::ProcessDoubleReplacing(
    double** inputs,
    double** outputs,
    int nFrames)
{
    // Mutex is already locked for us.

    double *leftOutput = outputs[0];
    double *rightOutput = outputs[1];

    for (int i = 0; i < nFrames; ++i) {
        mMIDIReceiver.advance();
				
        int velocity = mMIDIReceiver.getLastVelocity();
        if (velocity > 0) {
            mOscillator.setFrequency(mMIDIReceiver.getLastFrequency());
            mOscillator.setMuted(false);
        } else {
            mOscillator.setMuted(true);
        }
        leftOutput[i] = rightOutput[i] = mOscillator.nextSample() * velocity / 127.0;
    }
    mMIDIReceiver.Flush(nFrames);
		// DEBUG:For testing mMap
		if(!mMIDIReceiver.mMap.empty() && mMIDIReceiver.mMap.begin()->second.size() > 1) {
			MLT mMLT(&mMIDIReceiver.mMap);
			mMIDIReceiver.mapLock = true;
			if(mMLT.formatMap() && finishedSong == false) {

				long double tempo = 0;
				tempo = (long  double) mMLT.calcBruteForceTempo();
				// Keeping value within a range for DAW to not crash on too large of value
				//if(tempo > -1000000 && tempo < 1000000) {
					//sprintf(tmp, "%Lf", (long double) mMLT.lengthCount);
					long double tError = (long double) mMLT.tempoError;
					long double vError = (long double) mMLT.velocityError;
					char* tmp = (char*) malloc(30);
					char* tmp2 = (char*) malloc(30);
					sprintf(tmp, "Tempo Confidence level = %.3Lf%%", tError);
					sprintf(tmp2, "Velocity Confidence level = %.3Lf%%", vError);
					controlTwoMessage = tmp;
					controlThreeMessage = tmp2;
				 	setTempo(tempo);
				//}
				if(mMLT.lengthCount >= 64) {
					mMLT.writeSongMatrix();
					mMIDIReceiver.clearMap();
					//exit(EXIT_SUCCESS);
					finishedSong = true;
				}
				mMIDIReceiver.mapLock = false;
			}
		} 
}

void ReverseMetronome::Reset()
{
    TRACE;
    IMutexLock lock(this);
    mOscillator.setSampleRate(GetSampleRate());
		mMIDIReceiver.mMap.clear();
}
void ReverseMetronome::OnParamChange(int paramIdx)
{
    IMutexLock lock(this);
}

void ReverseMetronome::CreatePresets() {
    MakeDefaultPreset((char *) "-", kNumPrograms);
}

void ReverseMetronome::ProcessMidiMsg(IMidiMsg* pMsg) {
    mMIDIReceiver.onMessageReceived(pMsg);
}

/*
 * Sets controlOneMessage char* from int input.
 */
void ReverseMetronome::setTempo(long double tempo) {
		char* str = (char*) malloc(6);
		sprintf(str, "%.2Lf", tempo);
		controlOneMessage = str;
		updateDisplay();
}

/*
 * Update the control parameters of the GUI.
 */
void ReverseMetronome::updateDisplay() {
		IMutexLock lock(this);
		IGraphics* pGraphics = GetGUI();
		// Get text controls
		ITextControl *topControl = (ITextControl*) pGraphics->GetControl(controlOne);
		ITextControl *secondControl = (ITextControl*) pGraphics->GetControl(controlTwo);
		ITextControl *thirdControl = (ITextControl*) pGraphics->GetControl(controlThree);
		// Set char* in text controls
		topControl->SetTextFromPlug(controlOneMessage); 
		secondControl->SetTextFromPlug(controlTwoMessage);
		thirdControl->SetTextFromPlug(controlThreeMessage);
		/*
		char* temp = "Tempo = ";
		//strcat(temp, controlOneMessage);
		//strcat(temp, "; tempo standard error = ");
		//strcat(temp, controlTwoMessage);
		//strcat(temp, "; velocity standard error = ");
		//strcat(temp, controlThreeMessage);
		//strcat(temp, "\n");
		std::ofstream outputFile("output_from_tests.txt");
		if(outputFile.is_open()) {
			outputFile << temp;
			thirdControl->SetTextFromPlug(controlThreeMessage);
			outputFile.close();
		} else {
			thirdControl->SetTextFromPlug("Could not write to file.");
		}*/
}