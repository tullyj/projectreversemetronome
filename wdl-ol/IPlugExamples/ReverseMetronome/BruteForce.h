#pragma once
#include "MIDIReceiver.h"
//#include "MLUtils.h"
#include <cassert>
#include <math.h>
#include <boost/accumulators/accumulators.hpp> 
//#include <boost/accumulators/accumulators_fwd.hpp>
//#include <boost/accumulators/statistics/min.hpp> 
//#include <boost/accumulators/statistics/max.hpp> 
//#include <boost/accumulators/statistics/mean.hpp> 
#include <boost/accumulators/statistics/stats.hpp> 
//#include <boost/accumulators/statistics/sum.hpp> 
//#include <boost/accumulators/statistics/mean.hpp>
//#include <boost/accumulators/statistics/moment.hpp>
#include <boost/accumulators/statistics/variance.hpp> 
//#include <boost/accumulators/statistics/count.hpp> 
//#include <boost/accumulators/statistics/median.hpp> 

using namespace boost::accumulators;

class BruteForce
{
public:
	BruteForce(std::map<int, std::vector<MIDIReceiver::Note>> *_mMap);
	~BruteForce(void);
	void getPitchAverages();
	void convertToTempos();
	long double getTempo();
	long double getAverageError(std::map<int, accumulator_set<long double, stats< tag::variance > >> *map);
	std::map<int, std::vector<MIDIReceiver::Note>> *mMap;
	std::map<int, accumulator_set<long double, stats< tag::variance > >> statTimeMap;
	std::map<int, accumulator_set<long double, stats< tag::variance > >> statVelocityMap;
	std::map<int, long double> noteTempos;
	std::map<int, long double> noteVelocities;
	int segmentLength;
	int segmentElement;
	static const int MIN_LENGTH = 64;
	bool lengthFlag;
	//long double avgTimeError;
};

