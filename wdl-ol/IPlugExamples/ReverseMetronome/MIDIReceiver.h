#ifndef MIDIRECEIVER_H
#define MIDIRECEIVER_H

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wextra-tokens"
#include "IPlug_include_in_plug_hdr.h"
#pragma clang diagnostic pop

#include "IMidiQueue.h"
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <vector>
#include <map>
#include <sys/timeb.h>

class MIDIReceiver {
private:
    IMidiQueue mMidiQueue;
    static const int keyCount = 128;
    int mNumKeys; // how many keys are being played at the moment (via midi)
	// array of on/off for each key (index is note number)
    bool mKeyStatus[keyCount]; 
	

	//Note note;//Initialized the node struct to use in this .h as well
	int mLastNoteNumber;
    double mLastFrequency;
    int mLastVelocity;
    int mOffset;
    inline double noteNumberToFrequency(int noteNumber) { return 440.0 * pow(2.0, (noteNumber - 69.0) / 12.0); }

// only for the 1st Node
//void initNode(struct Node *head,int n){
//	head->mLastNoteNumber = -1;
//	head->next =NULL;
//}

public:
    MIDIReceiver() :
    mNumKeys(0),
    //mLastNoteNumber(-1)

    //mLastFrequency(-1.0),
    //mLastVelocity(0),
	// last three lines allow one note at a time (monophonic)
	// TODO - we want it to be polyphonic (multiple notes at a time)
    mOffset(0) {
        for (int i = 0; i < keyCount; i++) {
            mKeyStatus[i] = false;
        }
    };

    // Returns true if the key with a given index is currently pressed
    inline bool getKeyStatus(int keyIndex) const { return mKeyStatus[keyIndex]; }
    // Returns the number of keys currently pressed
    inline int getNumKeys() const { return mNumKeys; }
    // Returns the last pressed note number
    inline int getLastNoteNumber() const { return mLastNoteNumber; }
    inline double getLastFrequency() const { return mLastFrequency; }
    inline int getLastVelocity() const { return mLastVelocity; }
    void advance();
    void onMessageReceived(IMidiMsg* midiMessage);
    inline void Flush(int nFrames) { mMidiQueue.Flush(nFrames); mOffset = 0; }
    inline void Resize(int blockSize) { mMidiQueue.Resize(blockSize); }
	struct Note {
		int mNoteNumber;
		int mVelocity;
		long double mTime;
		//~Node(){};
		// Pavel Kardash Dec 1st
		// Added struct, or in other words the variables that previously were
		// used are now in a struct called Node
		Note() :
			mNoteNumber(-1), 
			mVelocity(0),
			mTime(0)
			{};
		Note(int mLastNote, int mLastVel, long double mT) : 
			mNoteNumber(mLastNote), 
			mVelocity(mLastVel),
			mTime(mT)
			{};
	};
	std::map<int, std::vector<Note>> mMap;
	void clearMap();
	bool mapLock;
};

#endif