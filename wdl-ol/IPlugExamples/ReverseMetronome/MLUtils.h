#pragma once
#include "ReverseMetronome.h"
#include <opencv2/core/core.hpp> 
#include <opencv2/ml/ml.hpp>
#include <iostream>
#include <fstream>
#include <ostream>

using namespace cv;

class MLUtils
{
public:
	MLUtils(void);
	~MLUtils(void);
	void setMatrixArrays(std::map<int, long double> *tempos);
	void setMatrixArrays(std::map<int, long double> *tempos, std::map<int, long double> *velocities);
	Mat getSongMatrix();
	void writeMatrixToFile();
	float* getTrainingMatrix();
	long double noteTempos[128];
	long double noteVelocities[128];
	bool arraysSet;
	bool velocitiesIncluded;
	char* filename;
	//std::ofstream outputFile;
};
