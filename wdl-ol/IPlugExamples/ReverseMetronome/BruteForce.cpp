#include "BruteForce.h"
#include <cmath>

BruteForce::BruteForce(std::map<int, std::vector<MIDIReceiver::Note>> *_mMap)
	: mMap(_mMap), segmentLength(-1), segmentElement(-1) {
		getPitchAverages();
		convertToTempos();
}

/*
 * Get and save the average time interval per each pitch.
 */
void BruteForce::getPitchAverages() {
		// Check that mMap isn't empty
		assert(!mMap->empty());
		long double currentTempoAverage = 0.000f;
		long double currentVelocityAverage = 0.000f;
		int currentLength = 0;
		//avgTimeError = 0.0;
		std::map<int, std::vector<MIDIReceiver::Note>>::iterator it = mMap->begin();
		//std::vector<double> timeVector;
		//std::vector<double> velocityVector;
		for(; it != mMap->end(); ++it) {
				boost::accumulators::accumulator_set<long double, stats< tag::variance > > accTime;
				boost::accumulators::accumulator_set<long double, stats< tag::variance > > accVelocity;
				currentTempoAverage = currentVelocityAverage = 0.00f;
				currentLength = it->second.size();
				if(currentLength > segmentLength) segmentLength = currentLength;
				std::vector<MIDIReceiver::Note>::iterator jt = it->second.begin();
				// Add first note's velocity to average
				currentVelocityAverage = (long double) (currentVelocityAverage + jt->mVelocity);
				accVelocity((long double)jt->mVelocity);
				// Skip the first element for tempo averages
				++jt;
				for(; jt != it->second.end(); ++jt) {
						// Sum up time intervals between noteTempos
						currentTempoAverage = (long double) (currentTempoAverage + jt->mTime);
						// Sum up velocities of noteTempos
						currentVelocityAverage = (long double) (currentVelocityAverage + jt->mVelocity);
						// Store time interval and velocity into accumulators
						accTime((long double)jt->mTime);
						accVelocity((long double)jt->mVelocity);
				}
				if(currentTempoAverage > 0.0) {
				// Save average time intervals of the current noteNumber (pitch)
				noteTempos[it->first] = (long double) currentTempoAverage/(it->second.size() - 1);
				// Save average velocities of the current noteNumber (pitch)
				noteVelocities[it->first] = (long double) currentVelocityAverage/(it->second.size() - 1);
				// Store accumulators into maps
				}
				statTimeMap[it->first] = (accTime);
				statVelocityMap[it->first] = (accVelocity);
		}
		if(segmentLength < MIN_LENGTH)
			lengthFlag = false;
		else
			lengthFlag = true;
}

/*
 * Convert from ms averages to a tempo (beats per minute)
 */
void BruteForce::convertToTempos() { 
		for(auto it = noteTempos.begin(); it != noteTempos.end(); ++it) {
				// Divide the amount of seconds in a minute 
				// by the number of ms in a second
				it->second = (60 / it->second);
		}
}

/*
 * Get the average bpm as an int across all pitches.
 */
long double BruteForce::getTempo() {
		long double average = 0.0;
		long double average2 = 0.0;
		int n = 0;
		for(auto it = noteTempos.begin(); it!= noteTempos.end(); ++it) {
			if(it->second >= 0.0 && it->second < 9999) {
					average += (long double) it->second;
					++n;
			}
		}

		//MLUtils mUtils = MLUtils();
		//mUtils.setMatrixArrays(&noteTempos, &noteVelocities);
		//return mUtils.noteTempos[68];

		if(lengthFlag)
			return (long double) average/n;
		else
			return (long double) -(n);
}

/*
 * Calculate the standard error (SE = (standard_deviation/sqrt(n)))
 * per each pitch, and return the average of all the standard errors.
 */
long double BruteForce::getAverageError(std::map<int, accumulator_set<long double, stats< tag::variance > >> *map) {
	long double stdDev, avg, sqrdN;
	avg = 0.0;
	for(auto it = map->begin(); it != map->end(); ++it) {
		stdDev = sqrt(variance(it->second));
		sqrdN = sqrt((double)mMap->at(it->first).size());
		// get the standard error and add it to avg
		avg = (long double) (avg + (stdDev / sqrdN));
	}
	// Convert to percentage
	avg = (long double) (100 - (avg * 100));
	return avg;
}

BruteForce::~BruteForce(void)
{
}
