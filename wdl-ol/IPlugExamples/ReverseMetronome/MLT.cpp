#include "MLT.h"

MLT::~MLT(void)
{
}

MLT::MLT(std::map<int, std::vector<MIDIReceiver::Note>> *_mMap) :
	mMap(_mMap),
	// Initialize timeLength for a minimum.
	timeLength(-1.000f){
		//mMap->begin()->second[0].mTime = -1.0;
		//formatMap();	
}
/*
 * Updates mTime to be the actual interval between a 
 * note of specific instruments (pitch)
 */
bool MLT::formatMap() {
	if(mMap->empty()) return false;
	std::map<int, std::vector<MIDIReceiver::Note>>::iterator it = mMap->begin();
	lengthCount = 0;
	//mMap->begin()->second[0].mTime = -2.0;
	for(; it != mMap->end(); ++it) {
			// first is noteNumber second is the vector of struct note
			// NOTE: this loop is going from the end to the beginning of the vector
			// set first mTime to zero
			//it->second.begin()->mTime = 0;
			for (int j = it->second.size() - 1; j > 0; --j) {
				// Store the amount of notes played for this pitch
				if(it->second.size() > lengthCount) lengthCount = it->second.size();
				// Get length of song segment by finding max time interval (mTime)
				if((j == it->second.size() - 1) && (it->second[j].mTime > timeLength)) {
					// Store the time of the longest element (to be compared with 0th element)
					timeLength = it->second[j].mTime;
					// Store the index of the longest time element
					lengthElement = it->first;
					// Store the amount of notes played for this pitch
					//lengthCount = j;
				}
			}
	}
	//if(lengthCount < MIN_LENGTH) return false;
	return true;
	//mMap->begin()->second[0].mTime = -3;
}

long double MLT::calcBruteForceTempo() {
		BruteForce bf(mMap);
		tempoError = bf.getAverageError(&bf.statTimeMap);
		velocityError = bf.getAverageError(&bf.statVelocityMap);
		return bf.getTempo();
}

void MLT::writeSongMatrix() {
	BruteForce bf(mMap);
	MLUtils mUtil = MLUtils();
	mUtil.setMatrixArrays(&bf.noteTempos);
	mUtil.writeMatrixToFile();
	//float *matrix = mUtil.getTrainingMatrix();
	/*Mat trainingDataMat(5, 128, CV_64FC1, &matrix);
	std::ofstream outputFile("C:\\MATRIXTEST.txt");
	for (int i = 0; i < 5; ++i)
	{
		for (int j = 0; j < 128; ++j)
		{
			outputFile << trainingDataMat.at<long double>(i,j);
		}
	}
	outputFile.close();
	std::ofstream outputFile("C:\\data_plz.txt");
	for (int i = 0; i < 5; ++i)
	{
		for (int j = 0; j < 128; ++j)
		{
			outputFile << ;
		}
	}
	outputFile.close();*/
}