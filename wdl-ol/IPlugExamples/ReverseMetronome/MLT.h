#pragma once
#include "MIDIReceiver.h"
#include "BruteForce.h"
#include "MLUtils.h"
#include <opencv2/core/core.hpp> 
#include <opencv2/ml/ml.hpp>

class MLT
{
public:
	//MLT(void) : mMap(NULL){};
	~MLT(void);
	MLT(std::map<int, std::vector<MIDIReceiver::Note>> *_mMap);

	std::map<int, std::vector<MIDIReceiver::Note>> *mMap;
	bool formatMap();
	long double calcBruteForceTempo();
	void writeSongMatrix();
	// Stores the length of the song segment MLT will analyze
	long double timeLength;
	int lengthElement;
	int lengthCount;
	long double tempoError;
	long double velocityError;
	static const int MIN_LENGTH = 64;
};

