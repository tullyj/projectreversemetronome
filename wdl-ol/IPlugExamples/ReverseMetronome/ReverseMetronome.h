#ifndef __REVERSEMETRONOME__
#define __REVERSEMETRONOME__

#include "IPlug_include_in_plug_hdr.h"
#include "Oscillator.h"
#include "MIDIReceiver.h"
#include "MLT.h"
#include <math.h>
#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <ostream>
#include <cassert>
#include <map>

class ReverseMetronome : public IPlug
{
public:
  ReverseMetronome(IPlugInstanceInfo instanceInfo);
  ~ReverseMetronome();

  void Reset();
  void OnParamChange(int paramIdx);
  void ProcessDoubleReplacing(double** inputs, double** outputs, int nFrames);
	// to receive MIDI messages:
	void ProcessMidiMsg(IMidiMsg* pMsg);
	void setTempo(long double tempo);
	// the string that will have tempo and will be displayed on the screen
	char* controlOneMessage;
	// string for second line of text on GUI
	char* controlTwoMessage;
	// string for third line of text on GUI
	char* controlThreeMessage;
	// message for telling user that tempo is being calculated
	char* calcMessage;

	void updateDisplay();
	static const int MAX_TEMPO = 360;
	static const int MIN_TEMPO = 0;
private:
  double mFrequency;
  void CreatePresets();
  Oscillator mOscillator;
  MIDIReceiver mMIDIReceiver;
  // the two lines of text desplayed
	int controlOne;
	int controlTwo;
	int controlThree;
	bool finishedSong;
};

#endif
