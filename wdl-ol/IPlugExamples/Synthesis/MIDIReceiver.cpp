#include "MIDIReceiver.h"


void MIDIReceiver::onMessageReceived(IMidiMsg* midiMessage) {
    IMidiMsg::EStatusMsg status = midiMessage->StatusMsg();
    // We're only interested in Note On/Off messages (not CC, pitch, etc.)
    if(status == IMidiMsg::kNoteOn || status == IMidiMsg::kNoteOff) {
		// if there is a note on or note off message then add to 
		// MIDI message queue
        mMidiQueue.Add(midiMessage);
    }
}

void MIDIReceiver::advance() {
    while (!mMidiQueue.Empty()) {
		// if we have a message (key on or off)

		// creating a pointer to midiMessage that was 
		// received/created on line 10 of this cpp file
		// and assigning to it last msg in the queue
        IMidiMsg* midiMessage = mMidiQueue.Peek();
		// if object of midiMessage called mOffset is greater then the 
		// current mOffset received, we don't want to receive it 
		// (old part that we already received).
		// TODO - what does the int mOffset stands for?
        if (midiMessage->mOffset > mOffset) break;

        IMidiMsg::EStatusMsg status = midiMessage->StatusMsg();
        int noteNumber = midiMessage->NoteNumber();
        int velocity = midiMessage->Velocity();
        // There are only note on/off messages in the queue, see ::OnMessageReceived
        if (status == IMidiMsg::kNoteOn && velocity) {
            if(mKeyStatus[noteNumber] == false) {
                mKeyStatus[noteNumber] = true;
                mNumKeys += 1;
            }
            // A key pressed later overrides any previously pressed key:
            if (noteNumber != mLastNoteNumber) {
                mLastNoteNumber = noteNumber;
                mLastFrequency = noteNumberToFrequency(mLastNoteNumber);
                mLastVelocity = velocity;
            }
        } else {
            if(mKeyStatus[noteNumber] == true) {
                mKeyStatus[noteNumber] = false;
                mNumKeys -= 1;
            }
            // If the last note was released, nothing should play:
            if (noteNumber == mLastNoteNumber) {
                mLastNoteNumber = -1;
                mLastFrequency = -1;
                mLastVelocity = 0;
            }
        }
        mMidiQueue.Remove();
    }
    mOffset++;
}