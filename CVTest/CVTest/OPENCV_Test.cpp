#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml/ml.hpp>
#include <iostream>
#include <fstream>
#include <ostream>

using namespace cv;

int main()
{
	//Mat M = Mat();
	std::ifstream matrixFile("C:\\output_from_tests.txt");
	int matrixRows = 0;
	float matrix[657][128];
	while(!matrixFile.eof()) {
		for(int i = 0; i < 128; ++i) {
			matrixFile >> matrix[matrixRows][i];
			if(matrixFile.eof())
			{
				break;
			}
		}
		if(matrixFile.eof())
		{
			break;
		}
		//M.push_back(Mat(128, 1, CV_64F, currentRow).inv());
		matrixRows = matrixRows + 1;
	}
	matrixFile.close();
	std::ifstream yFile("C:\\y_values.txt");
	float y_list[657][1];
	int yRows = 0;
	while(!yFile.eof()) {
		yFile >> y_list[yRows][1];
		if(yFile.eof())
		{
			break;
		}
		yRows = yRows + 1;
	}
	yFile.close();
	std::ifstream testFile("C:\\y_values.txt");
	float testData[2][128];
	int testRows = 0;
	while(!testFile.eof()) {
		for(int i = 0; i < 128; ++i) {
			testFile >> testData[testRows][i];
			if(testFile.eof())
			{
				break;
			}
		}
		if(testFile.eof())
		{
			break;
		}
		//M.push_back(Mat(128, 1, CV_64F, currentRow).inv());
		testRows = testRows + 1;
	}
	testFile.close();
	Mat trainingDataMat(matrixRows, 128, CV_32FC1, matrix);
	Mat answers(yRows, 1, CV_32FC1, y_list);
	Mat M = Mat();
	Mat sampleMat(testRows, 128, CV_32FC1, testData);

	CvKNearest KNearest;
	//exit(1);
	KNearest.train(trainingDataMat, answers, M, true, 3, false);
	float response;
	response = (float) KNearest.find_nearest(sampleMat, 3, 0, 0, 0, 0);
	//exit(1);
	
	
	/*
	CvSVMParams params;
    params.svm_type    = CvSVM::C_SVC;
    params.kernel_type = CvSVM::LINEAR;
    params.term_crit   = cvTermCriteria(CV_TERMCRIT_ITER, 100, 1e-6);
	CvSVM SVM;
	//exit(1);
    SVM.train(trainingDataMat, answers, Mat(), Mat(), params);
	exit(1);
	double response = SVM.predict(sampleMat);
	//exit(1);
	*/

	/*std::ofstream outputFile("C:\\GiveMe120.txt");
	for (int i = 0; i < 5; ++i)
	{
		for (int j = 0; j < 128; ++j)
		{
			outputFile << trainingDataMat.at<long double>(i,j);
		}
		outputFile << std::endl;
	}
	outputFile << "plz write here";
	outputFile.close();*/
	
	std::ofstream outputfile("c:\\giveme120.txt");
	outputfile << response << std::endl;
	outputfile.close();
	return 0;
	/*
    Vec3b green(0,255,0), blue (255,0,0);
    // Show the decision regions given by the SVM
    for (int i = 0; i < image.rows; ++i)
        for (int j = 0; j < image.cols; ++j)
        {
            Mat sampleMat = (Mat_<float>(1,2) << i,j);
            float response = SVM.predict(sampleMat);

            if (response == 1)
                image.at<Vec3b>(j, i)  = green;
            else if (response == -1)
                 image.at<Vec3b>(j, i)  = blue;
        }
	*/

	/*
    // Show the training data
    int thickness = -1;
    int lineType = 8;
    circle( image, Point(501,  10), 5, Scalar(  0,   0,   0), thickness, lineType);
    circle( image, Point(255,  10), 5, Scalar(255, 255, 255), thickness, lineType);
    circle( image, Point(501, 255), 5, Scalar(255, 255, 255), thickness, lineType);
    circle( image, Point( 10, 501), 5, Scalar(255, 255, 255), thickness, lineType);

    // Show support vectors
    thickness = 2;
    lineType  = 8;
    int c     = SVM.get_support_vector_count();

    for (int i = 0; i < c; ++i)
    {
        const float* v = SVM.get_support_vector(i);
        circle( image,  Point( (int) v[0], (int) v[1]),   6,  Scalar(128, 128, 128), thickness, lineType);
    }

    imwrite("result.png", image);        // save the image

    imshow("SVM Simple Example", image); // show it to the user
    waitKey(0);
	*/

}